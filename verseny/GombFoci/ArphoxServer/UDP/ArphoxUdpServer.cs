﻿using GameLogic;
using System;
using System.Threading.Tasks;

namespace ArphoxServer.UDP
{
    internal sealed class ArphoxUdpServer
    {
        private readonly GameController _gameController;
        public event EventHandler<string> EntryLogged;

        public ArphoxUdpServer(GameController gameController)
        {
            _gameController = gameController ?? throw new ArgumentNullException(nameof(gameController));
        }

        public void RunInBackground()
        {
            UdpServer udpServer = new UdpServer();
            udpServer.EntryLogged += EntryLogged;
            udpServer.MessageReceived += _gameController.ProcessUdpBlock;
            Task.Run(() => udpServer.StartListening());
        }
    }
}