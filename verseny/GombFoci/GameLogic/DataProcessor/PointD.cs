﻿namespace GameLogic.DataProcessor
{
    public sealed class PointD
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double OutputX { get; set; }
        public double OutputY { get; set; }

        public override string ToString() => $"X: {X} Y:{Y}, OutputX: {OutputX} OutputY: {OutputY}";

        public void MultiplyByScalar(double scalar)
        {
            X *= scalar;
            Y *= scalar;
            OutputX *= scalar;
            OutputY *= scalar;
        }
    }
}