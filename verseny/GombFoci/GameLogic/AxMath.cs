﻿namespace GameLogic
{
    public static class AxMath
    {
        public static double GetYByX(double TargetX, double TargetY, double SourceX, double SourceY, double x)
        {
            double part1 = (TargetY - SourceY) / (TargetX - SourceX);
            double part2 = x - TargetX;
            return part1 * part2 + TargetY;
        }

        public static bool IsGate(double y)
        {
            return y > (GameSettings.MapHeight - GameSettings.GateHeight) / 2 ||
                   y < GameSettings.MapHeight - ((GameSettings.MapHeight - GameSettings.GateHeight) / 2);
        }

        public static bool IsOverGate(double y)
        {
            return y <= (GameSettings.MapHeight - GameSettings.GateHeight) / 2;
        }

        public static bool IsUnderGate(double y)
        {
            return y >= GameSettings.MapHeight - ((GameSettings.MapHeight - GameSettings.GateHeight) / 2);
        }
    }
}