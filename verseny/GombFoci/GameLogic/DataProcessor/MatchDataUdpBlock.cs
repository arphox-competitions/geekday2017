﻿using System.Globalization;

namespace GameLogic.DataProcessor
{
    public sealed class MatchDataUdpBlock : UdpBlock
    {
        public PointD BallPosition { get; private set; }

        public PointD Player1Unit1Position { get; private set; }
        public PointD Player1Unit2Position { get; private set; }
        public PointD Player1Unit3Position { get; private set; }

        public PointD Player2Unit1Position { get; private set; }
        public PointD Player2Unit2Position { get; private set; }
        public PointD Player2Unit3Position { get; private set; }

        private MatchDataUdpBlock(int matchId)
            : base(matchId)
        { }

        public static MatchDataUdpBlock Parse(string[] message)
        {
            return new MatchDataUdpBlock(int.Parse(message[0]))
            {
                BallPosition = new PointD(BetterParse(message[1]), BetterParse(message[2])),
                Player1Unit1Position = new PointD(BetterParse(message[3]), BetterParse(message[4])),
                Player1Unit2Position = new PointD(BetterParse(message[5]), BetterParse(message[6])),
                Player1Unit3Position = new PointD(BetterParse(message[7]), BetterParse(message[8])),
                Player2Unit1Position = new PointD(BetterParse(message[9]), BetterParse(message[10])),
                Player2Unit2Position = new PointD(BetterParse(message[11]), BetterParse(message[12])),
                Player2Unit3Position = new PointD(BetterParse(message[13]), BetterParse(message[14]))
            };
        }

        private static double BetterParse(string d) => double.Parse(d.Replace(",", "."));
    }
}