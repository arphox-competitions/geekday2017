﻿using GameLogic.DataProcessor;
using System.Collections.Generic;
using System.Linq;

namespace GameLogic
{
    public static class MatchDataListExtensions
    {
        public static PointD GetAtMiddleByX(this List<PointD> points)
        {
            List<PointD> copy = points.ToList();
            copy = copy.OrderBy(p => p.X).ToList();
            return copy[copy.Count / 2];
        }

        public static PointD GetAtMiddleByY(this List<PointD> points)
        {
            List<PointD> copy = points.ToList();
            copy = copy.OrderBy(p => p.Y).ToList();
            return copy[copy.Count / 2];
        }

        public static void CopyValuesToOutputsOf(this List<PointD> outputs, List<PointD> units)
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].OutputX = outputs[i].X;
                units[i].OutputY = outputs[i].Y;
            }
        }
    }
}