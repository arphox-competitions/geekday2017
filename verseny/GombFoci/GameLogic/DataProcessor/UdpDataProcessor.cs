﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLogic.DataProcessor
{
    internal static class UdpDataProcessor
    {
        internal static List<UdpBlock> ProcessBlocks(byte[] data)
        {
            List<UdpBlock> typedBlocks = new List<UdpBlock>();

            List<byte[]> blocks = Tools.UdpDataFragmenter.FragmentUdpData(data);
            List<string> blockStrings = blocks.Select(b => Encoding.ASCII.GetString(b)).ToList();

            foreach (string block in blockStrings)
            {
                string[] splitted = block.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                switch (splitted.Length)
                {
                    case 15: typedBlocks.Add(MatchDataUdpBlock.Parse(splitted)); break;
                    case 5: typedBlocks.Add(MatchStatisticsUdpBlock.Parse(splitted)); break;
                    default: throw new Exception(); // TODO: put BREAK here in prod
                }
            }

            return typedBlocks;
        }
    }
}