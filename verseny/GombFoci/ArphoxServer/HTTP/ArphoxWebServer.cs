﻿using GameLogic;
using System;
using System.Diagnostics;
using System.Net;

namespace ArphoxServer.HTTP
{
    internal sealed class ArphoxWebServer
    {
        private readonly GameController _gameController;
        public event EventHandler<string> EntryLogged;

        public ArphoxWebServer(GameController gameController)
        {
            _gameController = gameController ?? throw new ArgumentNullException(nameof(gameController));
        }

        public void RunInBackground()
        {
            WebServer httpServer = new WebServer(ProcessMesssage, ServerSettings.HttpUrl);
            httpServer.EntryLogged += EntryLogged;
            httpServer.Run();
        }

        private string ProcessMesssage(HttpListenerRequest request)
        {
            int matchId = int.Parse(request.QueryString["match"]);
            int player = int.Parse(request.QueryString["player"]);

            Stopwatch sw = Stopwatch.StartNew();
            string response = _gameController.CreateHttpResponse(matchId, player);
            sw.Stop();

            string msg = $"REQUEST: matchId={matchId}, player={player}. RESPONSE sent back in {sw.ElapsedMilliseconds} ms: {response.Replace("\n", " ")}";
            EntryLogged?.Invoke(this, msg);

            return response;
        }
    }
}
