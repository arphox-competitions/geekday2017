﻿namespace GameLogic
{
    public static class GameSettings
    {
        public const int MapWidth = 1000;
        public const int MapHeight = 700;

        public const int BallDiameter = 30;
        public const int PlayerDiameter = 60;
        public const int GateHeight = 175;

        public const double MaxSpeed = 6.1;

        public static int MapHeightMidpoint => MapHeight / 2;
    }
}