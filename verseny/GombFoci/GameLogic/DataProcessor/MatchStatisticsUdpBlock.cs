﻿namespace GameLogic.DataProcessor
{
    public sealed class MatchStatisticsUdpBlock : UdpBlock
    {
        public int Player1Goals { get; private set; }
        public int Player1Points { get; private set; }
        public int Player2Goals { get; private set; }
        public int Player2Points { get; private set; }

        public MatchStatisticsUdpBlock(int matchId)
            : base(matchId)
        { }

        public static MatchStatisticsUdpBlock Parse(string[] message)
        {
            return new MatchStatisticsUdpBlock(int.Parse(message[0]))
            {
                Player1Goals = int.Parse(message[1]),
                Player1Points = int.Parse(message[2]),
                Player2Goals = int.Parse(message[3]),
                Player2Points = int.Parse(message[4])
            };
        }

        public static bool IsGoalScored(MatchStatisticsUdpBlock stat1, MatchStatisticsUdpBlock stat2)
        {
            if (stat1 == null || stat2 == null)
                return false;

            if (stat1.Player1Goals != stat2.Player1Goals)
                return true;

            if (stat1.Player2Goals != stat2.Player2Goals)
                return true;

            return false;
        }
    }
}