﻿using GameLogic.DataProcessor;
using System;
using System.Collections.Generic;

namespace GameLogic.Extensions
{
    public static class PointDExtensions
    {
        public static double GetDistance(this PointD p1, PointD p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public static double CalculateAngleTowards(this PointD self, PointD target)
        {
            double a = self.Y - target.Y;
            double b = self.X - target.X;
            double angle = Math.Atan(a / b) * 180 / Math.PI;
            if (target.X <= self.X)
                angle += 180;

            return angle;
        }

        public static PointD CalculateDirectionVector(this PointD self, PointD target)
        {
            double xDiff = target.X - self.X;
            double yDiff = target.Y - self.Y;
            return new PointD(xDiff, yDiff);
        }

        public static void NormalizeOutputs(this PointD p, double maxSpeed = GameSettings.MaxSpeed)
        {
            double bigger = Math.Max(Math.Abs(p.OutputX), Math.Abs(p.OutputY));

            if (bigger != 0)
            {
                p.OutputX /= bigger;
                p.OutputY /= bigger;
            }

            p.OutputX *= maxSpeed;
            p.OutputY *= maxSpeed;
        }

        public static PointD GetNearest(this PointD target, List<PointD> points)
        {
            int minindex = 0;
            double minDistance = double.MaxValue;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].X == target.X && points[i].Y == target.Y)
                    continue;

                double currentDistance = target.GetDistance(points[i]);
                if (currentDistance < minDistance)
                {
                    minDistance = currentDistance;
                    minindex = i;
                }
            }

            return points[minindex];
        }
    }
}