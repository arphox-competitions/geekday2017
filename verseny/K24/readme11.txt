Gooooooooooooooooooooood morning, campers!

Now that we're almost awake, let me share two things: 

1) the speed limits are now lifted - you can send speed vectors between [-maxSpeed; +maxSpeed] (stored in the db). Now you can set the maxSpeed to any values for testing.

2) Please note that speed matters: in a match, the players will instantly move after receiving the HTTP reply. So the user that replies the HTTP request first will have an advantage over the worse algorithm!

3) DB RESET. Removed matches and games (players remain). Please re-insert your match records into the DB.