Goodnight all :)

Feel free to stop and re-start the visual studio's debug session if it dies with an exception or if you want to force a full reload of the players table into the game (e.g. udp/http listener changes, picture changes).

Don't forget to move the program window into the beamer/projector screen after that.

If my computer goes to sleep, then the password is the same as the username ("anon")

Wake me up only if the building's on fire.
