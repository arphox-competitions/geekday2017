﻿using GameLogic.DataProcessor;
using GameLogic.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GameLogic
{
    public sealed class GameController
    {
        private Dictionary<int, ArphoxAI> ais = new Dictionary<int, ArphoxAI>();
        public event EventHandler<string> MessageLogger;

        public EventHandler<UdpBlockArrivedEventArgs> UdpBlockArrived;

        public void ProcessUdpBlock(object sender, byte[] data)
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<UdpBlock> blocks = UdpDataProcessor.ProcessBlocks(data);
            List<MatchStatisticsUdpBlock> statBlocks = blocks.OfType<MatchStatisticsUdpBlock>().ToList();
            List<MatchDataUdpBlock> dataBlocks = blocks.OfType<MatchDataUdpBlock>().ToList();

            foreach (UdpBlock block in blocks)
            {
                CreateAiIfNotExist(block.MatchId);
                ArphoxAI ai = ais[block.MatchId];
                switch (block)
                {
                    case MatchDataUdpBlock dataBlock:
                        ai.AcceptData(dataBlock);
                        UdpBlockArrived?.Invoke(this, new UdpBlockArrivedEventArgs(dataBlock.MatchId, dataBlock));
                        break;
                    case MatchStatisticsUdpBlock statBlock:
                        ai.AcceptStat(statBlock);
                        break;
                }
            }

            sw.Stop();

            string msg = $"UDP packet processed in {sw.ElapsedMilliseconds} ms with {dataBlocks.Count} data blocks and {statBlocks.Count} stat blocks.";
            MessageLogger?.Invoke(this, msg);

            //string dataInString = Encoding.ASCII.GetString(data);
            //File.AppendAllText(@"C:\temp\log.txt", dataInString + Environment.NewLine);
        }

        public string CreateHttpResponse(int matchId, int player)
        {
            CreateAiIfNotExist(matchId);
            return ais[matchId].CreateResponse(player);
        }

        private void CreateAiIfNotExist(int matchId)
        {
            if (!ais.ContainsKey(matchId))
            {
                ais.Add(matchId, new ArphoxAI(matchId));
                MessageLogger?.Invoke(this, $"Created AI for {matchId}.");
            }
        }
    }
}