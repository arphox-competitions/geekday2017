﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ArphoxServer.UDP
{
    internal sealed class UdpServer
    {
        internal event EventHandler<byte[]> MessageReceived;
        public event EventHandler<string> EntryLogged;

        public void StartListening()
        {
            UdpClient server = new UdpClient(new IPEndPoint(IPAddress.Any, ServerSettings.UdpPort));

            IPEndPoint remoteEndpoint = null;

            EntryLogged?.Invoke(this, $"UDP server started listenning on port {ServerSettings.UdpPort}.");

            while (true)
            {
                byte[] data = server.Receive(ref remoteEndpoint);

                string remoteIpAddress = remoteEndpoint.Address.ToString();
                if (!ServerSettings.WhiteListedIps.Contains(remoteIpAddress))
                {
                    EntryLogged?.Invoke(this, "Denied an UDP message from: " + remoteIpAddress);
                }
                else
                {
                    MessageReceived?.Invoke(this, data);
                }
            }
        }
    }
}